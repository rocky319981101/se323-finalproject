import { Component, OnInit } from '@angular/core';
import {StudentService} from '../service/student.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {

  constructor(
    public studentService: StudentService
  ) {
  }

  ngOnInit() {
  }

}
