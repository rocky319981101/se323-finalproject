import {Component, OnInit} from '@angular/core';
import {StudentService} from '../service/student.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ActivityService} from '../service/activity.service';
import {TeacherService} from '../service/teacher.service';
import {Location} from '@angular/common';
import {Activity, Student, Teacher} from '../model';

@Component({
  selector: 'app-teacher',
  templateUrl: './teacher.component.html',
  styleUrls: ['./teacher.component.scss']
})
export class TeacherComponent implements OnInit {
  constructor(
    public studentService: StudentService,
    public router: Router,
    public activityService: ActivityService,
    public teacherService: TeacherService,
    public location: Location,
    public route: ActivatedRoute
  ) {
  }

  teacher: Teacher | null = null;

  activities: Activity[] = [];

  activityIdToStudents = new Map<number,
    (Student & { state: 'pending' | 'confirm' })[]>();

  ngOnInit() {
    this.route.paramMap.subscribe(x => {
      this.teacherService.login(x.get('id')!);
      this.teacher = this.teacherService.currentTeacher;
      this.activities = this.activityService.activities.filter(
        x => x.teacherId === this.teacher!.id
      );
      this.activities.forEach(a => {
        const saList = this.activityService.studentActivityList.filter(
          x => x.activityId === a.id
        );
        const students = saList.map(x => ({
          ...this.studentService.students.find(y => y.id === x.studentId)!,
          state: x.state
        }));
        this.activityIdToStudents.set(a.id, students);
      });
    });
  }
}
