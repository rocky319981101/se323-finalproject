import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {IndexComponent} from './index/index.component';
import {StudentLoginComponent} from './student-login/student-login.component';
import {StudentRegComponent} from './student-reg/student-reg.component';
import {StudentComponent} from './student/student.component';
import {StudentActivityAllComponent} from './student-activity-all/student-activity-all.component';
import {StudentActivityEnrolledComponent} from './student-activity-enrolled/student-activity-enrolled.component';
import {StudentActivityConfirmedComponent} from './student-activity-confirmed/student-activity-confirmed.component';
import {TeacherComponent} from './teacher/teacher.component';
import {TeacherLoginComponent} from './teacher-login/teacher-login.component';
import {ManagerComponent} from './manager/manager.component';
import {AddActivityComponent} from './add-activity/add-activity.component';

const routes: Routes = [
  {
    path: '',
    component: IndexComponent
  },
  {
    path: 'student/login',
    component: StudentLoginComponent
  },
  {
    path: 'student/reg',
    component: StudentRegComponent
  },
  {
    path: 'student',
    component: StudentComponent
  },
  {
    path: 'student/activity/all',
    component: StudentActivityAllComponent
  },
  {
    path: 'student/activity/enrolled',
    component: StudentActivityEnrolledComponent
  },
  {
    path: 'student/activity/confirmed',
    component: StudentActivityConfirmedComponent
  },
  {
    path: 'teacher/:id',
    component: TeacherComponent
  },
  {
    path: 'teacher-login',
    component: TeacherLoginComponent
  },
  {
    path: 'manager',
    component: ManagerComponent
  },
  {
    path: 'activity/add',
    component: AddActivityComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
