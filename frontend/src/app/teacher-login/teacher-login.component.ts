import {Component, OnInit} from '@angular/core';
import {TeacherService} from '../service/teacher.service';

@Component({
  selector: 'app-teacher-login',
  templateUrl: './teacher-login.component.html',
  styleUrls: ['./teacher-login.component.scss']
})
export class TeacherLoginComponent implements OnInit {

  constructor(
    public teacherSrv: TeacherService
  ) {
  }

  ngOnInit() {
  }

}
