import {Component, OnInit} from '@angular/core';
import {ManagerService} from '../service/manager.service';
import {StudentService} from '../service/student.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ActivityService} from '../service/activity.service';
import {TeacherService} from '../service/teacher.service';
import {Location} from '@angular/common';
import {FormBuilder, Validators} from '@angular/forms';
import moment from 'moment';

@Component({
  selector: 'app-add-activity',
  templateUrl: './add-activity.component.html',
  styleUrls: ['./add-activity.component.scss']
})
export class AddActivityComponent implements OnInit {
  constructor(
    public managerService: ManagerService,
    public studentService: StudentService,
    public router: Router,
    public activityService: ActivityService,
    public teacherService: TeacherService,
    public location: Location,
    public fb: FormBuilder,
    public route: ActivatedRoute
  ) {
  }

  form = this.fb.group({
    name: this.fb.control('', Validators.required),
    location: this.fb.control('', Validators.required),
    description: this.fb.control('', Validators.required),
    period: this.fb.control(moment().format('YYYY-MM-DDTHH:mm'), [
      Validators.required
    ]),
    datetime: this.fb.control(
      moment().format('YYYY-MM-DDTHH:mm'),
      Validators.required
    ),
    teacherId: this.fb.control('', Validators.required)
  });

  ngOnInit() {
  }

  onSubmit(value: any) {
    this.managerService.addActivity(value);
    this.router.navigateByUrl('/manager');
  }
}
