import {Component, OnInit} from '@angular/core';
import {StudentService} from '../service/student.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ActivityService} from '../service/activity.service';
import {TeacherService} from '../service/teacher.service';
import {Location} from '@angular/common';
import {Activity, Student} from '../model';
import matchSorter from 'match-sorter';
import moment from 'moment';
import {ManagerService} from '../service/manager.service';

@Component({
  selector: 'app-manager',
  templateUrl: './manager.component.html',
  styleUrls: ['./manager.component.scss']
})
export class ManagerComponent implements OnInit {
  constructor(
    public managerService: ManagerService,
    public studentService: StudentService,
    public router: Router,
    public activityService: ActivityService,
    public teacherService: TeacherService,
    public location: Location,
    public route: ActivatedRoute
  ) {
    this.activities = activityService.activities.slice();
    const {confirmed, unconfirmed} = managerService.allStudents();
    this.confirmedStudents = confirmed;
    this.unconfirmedStudents = unconfirmed;
  }

  filter = '';

  beginTime = '';
  endTime = '';

  activities: Activity[] = [];

  confirmedStudents: Student[] = [];
  unconfirmedStudents: Student[] = [];

  ngOnInit() {
  }

  onInputFilter(value: string) {
    this.filter = value.trim();
    this.activities = matchSorter(
      this.activityService.activities,
      this.filter,
      {keys: ['name']}
    );
    this.filterByTimeRange();
  }

  onInputBeginTime(value: string) {
    console.log('onInputBeginTime', value);
    this.beginTime = moment(value.trim()).format('YYYY-MM-DD');
    this.filterByTimeRange();
  }

  onInputEndTime(value: string) {
    console.log('onInputEndTime', value);
    this.endTime = moment(value.trim()).format('YYYY-MM-DD');
    this.filterByTimeRange();
  }

  filterByTimeRange() {
    const b = this.beginTime;
    const e = this.endTime;
    let bt: moment.Moment, et: moment.Moment;
    try {
      if (b) {
        bt = moment(b);
      } else {
        bt = moment('1900-01-01');
      }
    } catch (e) {
      bt = moment('1900-01-01');
    }
    try {
      if (e) {
        et = moment(e + ' 23:59');
      } else {
        et = moment('2900-01-01');
      }
    } catch (e) {
      et = moment('2900-01-01');
    }
    console.log(bt.format(), et.format());
    this.activities = matchSorter(
      this.activityService.activities,
      this.filter,
      {keys: ['name']}
    );
    this.activities = this.activities.filter(x => {
      return (
        bt.toDate().getTime() <=
        moment(x.datetime)
          .toDate()
          .getTime() &&
        moment(x.datetime)
          .toDate()
          .getTime() <= et.toDate().getTime()
      );
    });
  }

  apply(activity: Activity) {
    if (window.confirm(`Are you sure to apply for this activity?`)) {
      this.activityService.apply(activity, this.studentService.loginStudent!);
    }
  }
}
