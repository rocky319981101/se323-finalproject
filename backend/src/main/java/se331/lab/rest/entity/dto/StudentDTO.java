package se331.lab.rest.entity.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import se331.lab.rest.entity.Student;
import se331.lab.rest.security.entity.User;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StudentDTO {
  Long id;
  String stid;
  String dob;
  String name;
  String surname;
  String imgurl;
  User user;

  public Student getStudent() {
    return Student.builder()
      .id(this.id)
      .stid(this.stid)
      .name(this.name)
      .surname(this.surname)
      .user(this.user)
      .imgurl(this.imgurl)
      .dob(this.dob)
      .build();
  }
  public static StudentDTO getStudentListDTO(Student student) {
    return StudentDTO.builder()
      .id(student.getId())
      .stid(student.getStid())
      .name(student.getName())
      .surname(student.getSurname())
      .imgurl(student.getImgurl())
      .dob(student.getDob())
      .user(student.getUser())
      .build();
  }
}
