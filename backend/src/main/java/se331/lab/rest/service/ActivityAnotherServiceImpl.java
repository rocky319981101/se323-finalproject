package se331.lab.rest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import se331.lab.rest.dao.ActivityAnotherDao;
import se331.lab.rest.dao.TeacherAnotherDao;
import se331.lab.rest.entity.Activity;
import se331.lab.rest.entity.Teacher;

import java.util.List;

@Service
public class ActivityAnotherServiceImpl implements ActivityAnotherService {
    private final
    ActivityAnotherDao activityAnotherDao;
    @Autowired
    TeacherAnotherDao teacherAnotherDao;
    @Autowired
    public ActivityAnotherServiceImpl(ActivityAnotherDao activityAnotherDao) {
        this.activityAnotherDao = activityAnotherDao;
    }

    @Transactional
    @Override
    public List<Activity> getActivities() {
        return activityAnotherDao.findAll();
    }

    @Override
    @Transactional
    public Activity save(Activity activity) {
        Teacher teacher = teacherAnotherDao.getTeacherById(activity.getLecturer().getId());
        activity.setLecturer(teacher);
        return activityAnotherDao.save(activity);
    }
}
