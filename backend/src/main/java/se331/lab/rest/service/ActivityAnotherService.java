package se331.lab.rest.service;

import se331.lab.rest.entity.Activity;

import java.util.List;

public interface ActivityAnotherService {
    List<Activity> getActivities();

    Activity save(Activity activity);

}

